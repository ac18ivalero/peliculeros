<?php

use Illuminate\Support\Facades\Response;
use Psr\Container\ContainerInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

class ControlerCrearSession
{
    private $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    public function __invoke(ServerRequestInterface $request, ResponseInterface $response, array $args): ResponseInterface
    {
        $datos = $request->getParsedBody();
        if (!isset($_SESSION['usuario']) && $datos["usuario"] == "admin" && $datos["contraseña"] == "admin") {
            $_SESSION["usuario"] = $datos["usuario"];
            return $response->withHeader('Location', '/')->withStatus(302);
        }
        return $response->withHeader('Location', '/sesion?funcion=3')->withStatus(302);
    }
}
