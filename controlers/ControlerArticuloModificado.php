<?php

use Psr\Container\ContainerInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use agenda\Models\Pelis as Pelis;

class ControlerArticuloModificado
{
    private $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    public function __invoke(ServerRequestInterface $request, ResponseInterface $response, array $args): ResponseInterface
    {
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            $datos = $request->getParsedBody();
            $Pelicula = Pelis::find($args['id']); //Todos los datos que sean igual que la id
            if (!is_null($Pelicula)) {
                print("LO ENCONTRO!");
                $Pelicula->nompeli = $datos["nompeli"];
                $Pelicula->director = $datos["director"];
                $Pelicula->intro = $datos["introduccion"];
                $Pelicula->argumento = $datos["argumento"];
                $Pelicula->fotourl = "/img/" . $_FILES['fotourl']['name'];
                $Pelicula->categoria_id = $datos["cotegoria_id"];
                $Pelicula->nomurl = strtolower($datos['nompeli']);
                $Pelicula->trailerurl = $datos["trailerurl"];
                try {
                    $Pelicula->save($Pelicula);
                    move_uploaded_file($_FILES['fotourl']['tmp_name'], "img/" . $_FILES['fotourl']['name']);
                    $out['articulo'] = $Pelicula;
                } catch (\Exception $e) {
                    $out['error'] = true;
                    $out['message'] = $e->getMessage();
                }
                $response->getBody()->write(json_encode($out));
            }
            print("NO ENCONTRO!");
        }
        return $response->withHeader('Content-Type', 'application/json');
    }
}
