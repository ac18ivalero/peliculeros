<?php

use agenda\Models\nomCategoria;
use Psr\Container\ContainerInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

class ControlerModificarArticulo
{
  private $container;

  public function __construct(ContainerInterface $container)
  {
    $this->container = $container;
  }

  public function __invoke(ServerRequestInterface $request, ResponseInterface $response, array $args): ResponseInterface
  {
    $nompeli = $args['nompeli'];
    $articulo = nomCategoria::where("nompeli", $nompeli)->get()[0];

    $data = ["pelis" => $articulo, "categoria" => strtoupper($nompeli)];

    return $this->container->get("view")->render($response, "articuloModificar.html.twig", $data);
  }
}
