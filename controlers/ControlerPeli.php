<?php


use agenda\Models\Pelis as Pelis;
use Psr\Container\ContainerInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

class ControlerPeli
{
    private $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    public function __invoke(ServerRequestInterface $request, ResponseInterface $response, array $args): ResponseInterface
    {
        $articulos = ["articulos" => Pelis::all()];
        return $this->container->get("view")->render($response, "articulo.html.twig", $articulos);
    }
}
