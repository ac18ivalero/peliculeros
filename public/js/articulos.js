var datos = document.querySelectorAll('#delete');

for (i = 0; i < datos.length; i++) {
  datos[i].addEventListener("click", function (obj) {
    borrarArticulo(obj.target.dataset.id, obj.target.parentElement.parentElement);
  });
}

function borrarArticulo(id, fila){
  var xmlhttp = new XMLHttpRequest();
  xmlhttp.onreadystatechange = function(){
    if(this.readyState == 4 && this.status == 200){
      var estado = JSON.parse(this.responseText);
      if(!estado.error){
        borrarFila(fila);
      }else{
        mensage(estado.message);
      }
    }
  }
  xmlhttp.open("DELETE", "/articulo/"+id, true);
  xmlhttp.send();
}

function borrarFila(fila) {
  var o = 1.0;
  function disminuir() {
    o -= 0.1;
    if (o > 0) {
      fila.style.opacity = o;
      window.setTimeout(disminuir, 50);
    } else {
      fila.remove();
    }
  }
  disminuir();
}

function mensage(msg) {
  alert(msg);
}
