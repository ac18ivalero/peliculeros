// Modificar Articulo
tinymce.init({
    selector: '#argumento',
});
tinymce.init({
    selector: '#introduccion',
});
const formulario = document.querySelector("#form_ModificarArticulo");

formulario.addEventListener("submit", function (event) {
    event.preventDefault();
    tinyMCE.triggerSave();

    let form = document.querySelector("#form_ModificarArticulo");
    let datos = new FormData(form);
    console.log([...datos]);

    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
            var resp = this.responseText;
            if (!resp.error) {
                alert("Articulo modificado.");
                location.href = "/articulos";
            } else {
                mensage(resp.message);
            }
        }
    };
    xhttp.open("POST", "/modificadoArticulo", true);
    xhttp.send(datos);
});
