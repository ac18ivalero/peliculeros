function verificarSession(){
  let liIniciarSession = document.querySelectorAll(".navbar-nav")[1].querySelectorAll(".nav-item")[1];
  let liCerrarSession = document.querySelectorAll(".navbar-nav")[1].querySelectorAll(".nav-item")[0];
  if(sessionStorage.getItem("usuario")){
      liCerrarSession.style.display = "block";
      liIniciarSession.style.display = "none";
  }else{
      liCerrarSession.style.display = "none";
      liIniciarSession.style.display = "block";
  }
}

const mostrarError = () => {
  var error = document.getElementById("error");
  let actual = window.location+'';
  let split = actual.split("=");
  let funcion = split[split.length-1];
  if(funcion == 1){
    error.classList.add("active");
    error.innerText = "¡Para gestionar artículos debes iniciar sesión!"
  }else if(funcion == 2){
    eliminarSession();
  }else if(funcion == 3){
    error.classList.add("active");
    error.innerText = "¡Usuario o contraseña incorrecto!"
  };
}

function crearSession(){
  let nombre = document.getElementById("usuario").value;
  let pass = document.getElementById("contraseña").value;
  if(nombre == "admin" && pass == "admin"){
      sessionStorage.setItem("usuario", nombre);
  }
}

function eliminarSession(){
  sessionStorage.removeItem("usuario");
}

mostrarError();
verificarSession();