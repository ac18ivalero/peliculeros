/* MI PARTE! */

tinymce.init({
  selector: '#argumento'
});
tinymce.init({
  selector: '#introduccion'
});

const formulario = document.querySelector("#form-nuevoArticulo");

formulario.addEventListener("submit", function(event){
    event.preventDefault();
    tinyMCE.triggerSave();

    let form = document.querySelector("#form-nuevoArticulo");
    let datos = new FormData(form);
    console.log([...datos]);

    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
      if(this.readyState == 4 && this.status == 200) {
        var resp = this.responseText;
        if(!resp.error){
          if(confirm("Nuevo articulo añadido. \nQuieres añadir un nuevo articulo?")){
            form.reset();
          }else{
            location.href="/articulos";
          }
        }else{
          mensage(resp.message);
        }
      }
    };
    xhttp.open("POST", "/subirArticulo", true);
    xhttp.send(datos);
});
