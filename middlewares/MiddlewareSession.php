<?php

namespace agenda\middleware;

use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Server\RequestHandlerInterface as RequestHandler;
use Slim\Psr7\Response;

class MiddlewareSession
{
    private $twig;

    public function __construct($twig)
    {
        $this->twig = $twig;
    }
    /**
     * Example middleware invokable class
     *
     * @param  ServerRequest  $request PSR-7 request
     * @param  RequestHandler $handler PSR-15 request handler
     *
     * @return Response
     */
    public function __invoke(Request $request, RequestHandler $handler): Response
    {
        $response = $handler->handle($request);
        return (isset($_SESSION['usuario'])) ? $response : $response->withHeader('Location', '/sesion?funcion=1')->withStatus(302);
    }
}
